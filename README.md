# v-non-overflow

## Remaining things to finish

Beautify layout with CSS

Add validation using v-validate

Add animation

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```
